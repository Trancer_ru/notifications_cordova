/**
 * Automatically generated file. DO NOT MODIFY
 */
package ru.notes.general.app;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "ru.notes.general.app";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 10006;
  public static final String VERSION_NAME = "1.0.0";
}
